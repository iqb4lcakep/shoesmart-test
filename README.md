# Test Shoesmart

Name : Muhammad Iqbal Rofikurrahman

## Usage
- Clone this repository
- at `main.go` uncomment the test section per number 

```bash
go run main.go

===== example output =====
Node 1 = 2  3  4 
Node 2 = 5  6  7 
Node 3 = 8  9  10 
Node 4 = 11  12  13 
```

