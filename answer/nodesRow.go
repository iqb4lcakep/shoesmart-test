package answer

import "fmt"

func ShowNodesAt(max_parrent int) {
	node_count := 3
	parrent := 1
	max := 1

	for parrent <= max_parrent {
		fmt.Printf("Node %d =", parrent)
		next_max := max + node_count

		for i := max; i < next_max; i++ {
			fmt.Printf(" %d ", i+1)
		}

		max = next_max
		parrent += 1
		fmt.Printf("\n")

	}

}
