package answer

import (
	"fmt"
	"strconv"
)

func BinaryConvert(n interface{}) {
	switch n.(type) {
	case int:
		fmt.Printf("The binary value is = '%b'\n", n)
	case string:
		n_str := n.(string)
		output, err := strconv.ParseInt(n_str, 2, 64)
		if err != nil {
			fmt.Println(err)
			return
		}

		fmt.Printf("The decimal value is = %d\n", output)
	}
}
