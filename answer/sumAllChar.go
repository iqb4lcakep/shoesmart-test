package answer

import (
	"fmt"
	"strings"
)

func SumAllChar(char string) {
	res := make(map[string]int)
	for i := range char {
		res[strings.ToLower(string(char[i]))] += 1
	}

	// iterate the results
	fmt.Printf("Output: ")
	for k, v := range res {
		fmt.Printf(" %s = %d,", k, v)
	}
	fmt.Printf("\n")

}
