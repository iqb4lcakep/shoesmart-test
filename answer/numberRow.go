package answer

import "fmt"

func ShowNumberRow(n int) {

	last_total := 1
	for i := 1; i <= n; i++ {
		last_total *= i
		fmt.Printf("%d ", last_total)
	}
	fmt.Printf("\n")
}
